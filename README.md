# Bevy Web Mouse Example

Example demonstrating mouse movement events and pointer lock.

Based roughly on https://github.com/mrk-its/bevy_webgl2_app_template

[Demo](https://sccarey.gitlab.io/bevy-web-mouse-example)

In the browser, open the developer console to see mouse movement events.
Click on the canvas to activate pointer lock. 
Hit the escape key to cancel pointer lock.
You may need to click/escape more than once.

Run in browser:
```
cargo make serve
```

As of 12/30/2020, the web version works best for Chrome or Edge. 
Firefox seems to have a bug causing spurious mouse movement events when pointer lock is enabled.

See .gitlab-ci.yml for additional prerequisite setup.