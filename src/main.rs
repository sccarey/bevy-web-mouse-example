use bevy::prelude::*;
mod utils;

mod plugins;

#[bevy_main]
fn main() {
    App::build()
        .add_plugins(bevy_webgl2::DefaultPlugins)
        .add_plugin(plugins::UiPlugin)
        .add_plugin(plugins::PlayerPlugin)
        .run();
}
