use crate::utils::html_body;
use bevy::input::mouse::MouseButtonInput;
use bevy::prelude::*;

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.init_resource::<TrackInputState>()
            .add_system(capture_mouse_on_click.system());
    }
}

#[derive(Default)]
struct TrackInputState {
    mousebtn: EventReader<MouseButtonInput>,
}

fn capture_mouse_on_click(
    mut state: ResMut<TrackInputState>,
    ev_mousebtn: Res<Events<MouseButtonInput>>,
) {
    for _ev in state.mousebtn.iter(&ev_mousebtn) {
        html_body::get().request_pointer_lock();
        break;
    }
}
