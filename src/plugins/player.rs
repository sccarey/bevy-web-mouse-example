use crate::utils::html_body;
use bevy::prelude::*;
use bevy_webgl2::renderer::JsCast;
use gloo::events::EventListener;
use std::sync::{
    atomic::{AtomicI32, Ordering::SeqCst},
    Arc,
};
use web_sys::MouseEvent;

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system(get_mouse_movement.system())
            .add_resource(WasmMouseTracker::new());
    }
}

pub struct WasmMouseTracker {
    delta_x: Arc<AtomicI32>,
    delta_y: Arc<AtomicI32>,
}

impl WasmMouseTracker {
    pub fn new() -> Self {
        let delta_x = Arc::new(AtomicI32::new(0));
        let delta_y = Arc::new(AtomicI32::new(0));

        let dx = Arc::clone(&delta_x);
        let dy = Arc::clone(&delta_y);

        // From https://www.webassemblyman.com/rustwasm/how_to_add_mouse_events_in_rust_webassembly.html
        let on_move = EventListener::new(&html_body::get(), "mousemove", move |e| {
            let mouse_event = e.clone().dyn_into::<MouseEvent>().unwrap();
            dx.store(mouse_event.movement_x(), SeqCst);
            dy.store(mouse_event.movement_y(), SeqCst);
        });
        on_move.forget();
        Self { delta_x, delta_y }
    }

    pub fn get_delta_and_reset(&self) -> Vec2 {
        let delta = Vec2::new(
            self.delta_x.load(SeqCst) as f32,
            self.delta_y.load(SeqCst) as f32,
        );
        self.delta_x.store(0, SeqCst);
        self.delta_y.store(0, SeqCst);
        delta
    }
}

pub fn get_mouse_movement(wasm_mouse_tracker: Res<WasmMouseTracker>) {
    let delta = wasm_mouse_tracker.get_delta_and_reset();
    if delta != Vec2::zero() {
        info!("Mouse movement: ({:?})", delta);
    }
}
